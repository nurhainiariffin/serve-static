process.env.NODE_ENV = process.env.NODE_ENV || "development";
const express = require("express");
const bodyParser = require("body-parser");
// const axios = require("axios");
const port = parseInt(process.env.PORT, 10) || 7000;
const app = express();
const path = require('path');

// ====Optimization====
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set("trust proxy", "loopback");
app.disable("x-powered-by");

// app.use(expressSanitized.middleware());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, x-access-token');

  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use('/assets', express.static(path.join(__dirname, '/assets')))

app.listen(port, (err) => {
    if (err) throw err;
    console.log(`Ready on http://localhost:${port}`);
});
  